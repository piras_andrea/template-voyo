<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Voyo | Responsive Multipurpose HTML5 Template</title>

	<!-- Change the author and description -->
	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Voyo One is a multipurpose HTML Template developed with the the latest HTML5 and CSS3 technologies. It can be perfectly fit for any corporate, e-commerce, business, agency or individual website.">



  	<!-- CSS files -->
	<link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600'>
	<link rel="stylesheet" href="inc/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="inc/animate.css">


	<link rel="stylesheet" href="inc/rs-plugin/css/settings.css" media="screen">


	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css">





	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">


</head>
<body>





<!-- Global Wrapper -->
<div id="wrapper">


	<!-- Header -->
	<?php include("php/header.php"); ?>

	<section class="privati-main-section custom-bg parallax rs_fullwidth" style="margin-top: -64.2px">
		<div class="custom-bg donare-main-image-container pt20 pb20" data-bg="dist/img/abitare.jpg">
			<div class="row mr0">
				<div class="col-sm-12 col-md-12 pad50" style="height:40%">
					<div class="pad50 white">
					<h4 class="white">CITTADINI</h4>
					<h2><strong class="white">Ci sono tanti modi <br> per entrare in contatto <br> con noi</strong></h2>
					</div>
				</div>
				<div class="col-sm-12 col-md-12 pad50 darkgray text-center"><!-- bootstrap column adaption-->
					<div class="col-sm-12 col-md-3">
						<div class="blog-item">
							<div class="blog-caption">
								<h3 class="post-title bt3">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
								<p class="sub-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!</p>
								<a href="#" class="btn">Continua</a>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-3">
						<div class="blog-item">
							<div class="blog-caption">
								<h3 class="post-title bt3">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
								<p class="sub-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!</p>
								<a href="#" class="btn">Continua</a>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-3">
						<div class="blog-item">
							<div class="blog-caption">
								<h3 class="post-title bt3">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
								<p class="sub-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!</p>
								<a href="#" class="btn">Continua</a>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-3">
						<div class="blog-item">
							<div class="blog-caption">
								<h3 class="post-title bt3">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
								<p class="sub-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!</p>
								<a href="#" class="btn">Continua</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	
	<section class="section6 pt50 pb50">
		<div class="container">
			<div >
				<h4>Energie Sociali</h4>
				<p>
		Ut nec turpis malesuada, porta augue nec, venenatis dolor. Aliquam ut vehicula eros. Nunc dapibus ante vel sapien commodo, ac pulvinar ex convallis. Aliquam pharetra hendrerit turpis sed placerat. Integer eu nunc ac tellus condimentum pulvinar. Ut sed diam id purus laoreet condimentum. Aliquam leo nibh, fermentum non iaculis quis, aliquam eu elit. Quisque tristique lectus at aliquam pretium. Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse odio dolor, consequat ut aliquet at, suscipit eu sem. Aliquam tempus tristique mauris, sit amet eleifend ante iaculis et. Nullam in sagittis ipsum. Cras volutpat pellentesque egestas. Etiam sit amet leo mattis, consequat leo ac, laoreet augue. Nam volutpat magna et posuere faucibus.
				</p>
			</div>
		</div>
	</section>

	
	
	<?php include_once('php/footer.php'); ?>
	
			</div> <!-- END Global Wrapper -->
			<!-- Javascript files -->
			<script src="dist/js/jquery.min.js"></script>
			<script src="dist/js/bootstrap.min.js"></script>
			<script src="dist/js/jquery.flexslider-min.js"></script>
			<script src="dist/js/jquery.appear.min.js"></script>
			<script src="dist/js/retina.min.js"></script>
			<script src="dist/js/jquery.countTo.min.js"></script>
			<!-- Main javascript file -->
			<script src="dist/js/script.min.js"></script>
		</body>
	</html>
