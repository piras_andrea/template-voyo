<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Voyo | Responsive Multipurpose HTML5 Template</title>

	<!-- Change the author and description -->
	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Voyo One is a multipurpose HTML Template developed with the the latest HTML5 and CSS3 technologies. It can be perfectly fit for any corporate, e-commerce, business, agency or individual website.">




  	<!-- CSS files -->
	<link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600'>
	<link rel="stylesheet" href="inc/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="inc/animate.css">




	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css">




	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">


</head>
<body>





<!-- Global Wrapper -->
<div id="wrapper" class="page8">

	<!-- Header -->
	<?php include("php/header.php"); ?>
	<!-- Page Header -->
<header class="titlebar" style="background-image: url(dist/img/titlebar.jpg); background-size:cover;"></header>
<section class="breadcrumbs breadcrumb_container" style="background:#ea5f5c;">
	<div class="container">
		<div class="row">
			<ol class="breadcrumb by ">
			  <li><a href="#">Home</a></li>
			  <li class="active">#News</li>
			</ol>
		</div>
	</div>
</section>
	<section class="mt50">

		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-8 col-md-push-4 space-left">
					<h1 class="main_title pink" >#News<br>
						<i><small>Sottotitoli</small></i>
					</h1>
					<div id="portfolio-iportfoliosotope">
								<div class="col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_2.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn-block btn_green">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_3.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn_green btn-block">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
								<div class=" col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_4.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn_green btn-block">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
							</div> <!-- END Row -->
							<div class="row">
								<div class="col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_2.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn-block btn_green">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_3.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn_green btn-block">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
								<div class=" col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_4.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn_green btn-block">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
							</div> <!-- END Row -->
							<div class="row">
								<div class="col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_2.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn-block btn_green">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_3.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn_green btn-block">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
								<div class=" col-sm-12 col-md-4  animated" data-anim="fadeInUp">
									<div class="blog-item">
										<div><img src="dist/img/le_storie_4.jpg" alt="service"></div>
											<div class="blog-caption pad0">
												<a href="#" class="btn_green btn-block">Nicola <br> Centro Parsifal</a>
											</div>
									</div>
								</div>
							</div> <!-- END Portfolio -->
						
						<div> <!--Begin Pagination -->
						
						<nav>
						  <ul class="pagination">
							<li class="page-item">
							  <a class="page-link" href="#" aria-label="Previous">
								<span aria-hidden="true"><i class="fa fa-arrow-left"></i></span>
								<span class="sr-only">Previous</span>
							  </a>
							</li>
							<li class="page-item active"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item">
							  <a class="page-link" href="#" aria-label="Next">
								<span aria-hidden="true"><i class="fa fa-arrow-right"></i></span>
								<span class="sr-only">Next</span>
							  </a>
							</li>
						  </ul>
						</nav>
						
						</div><!--End Pagination -->
						
						
						
						
						
					</div> <!-- END Portfolio Isotope -->

					<!--<ul class="pagination">
						<li class="disabled"><a href="#"><i class="fa fa-chevron-left"></i></a></li>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
					</ul>-->

				
				<div class="col-sm-12 col-md-4 col-md-pull-8 pad50">
					<aside class="sidebar">
						<div class="sidebar-widget">
							<div class="input-group">
							  <input type="text" class="formcontrol pb_gc_wb_imp" style="margin-bottom:0;" placeholder="Cerca" aria-describedby="basic-addon2">
							  <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search white"></i></span>
							</div>
							<h1 class="sidebar-title mt30 mb30 pink">Categorie</h1>
							<ul class="categories list-group">
								<li class="list-group-item"><a href="#">Animazione di strada</a><span class="badge">12</span></li>
								<li class="list-group-item"><a href="#">Centri giovanili - Casa Novarini</a><span class="badge">9</span></li>
								<li class="list-group-item"><a href="#">Interventi di prevenzione</a><span class="badge">23</span></li>
								<li class="list-group-item"><a href="#">informazione e orientamento</a><span class="badge">15</span></li>
								<li class="list-group-item"><a href="#">supporto scolastico e doposcuola</a><span class="badge">8</span></li>
								<li class="list-group-item"><a href="#">orineta lavoro</a><span class="badge">6</span></li>
								<li class="list-group-item"><a href="#">formazione e laboratori a scuola</a><span class="badge">1</span></li>
							</ul>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</section> <!-- END Blog Page-->
	
	


</div> <!-- END Global Wrapper -->





	<!-- Javascript files -->
	<script src="inc/jquery/jquery-2.1.0.min.js"></script>
	<script src="inc/bootstrap/js/bootstrap.min.js"></script>
	<script src="inc/jquery.appear.js"></script>
	<script src="inc/retina.min.js"></script>
	<script src="inc/jflickrfeed.min.js"></script>



	<script src="inc/isotope/isotope.pkgd.min.js"></script>
	<script src="inc/isotope/imagesloaded.pkgd.min.js"></script>


	<!-- Main javascript file -->
	<script src="js/script.js"></script>


</body>
</html>
