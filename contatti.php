<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Voyo | Responsive Multipurpose HTML5 Template</title>

	<!-- Change the author and description -->
	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Voyo One is a multipurpose HTML Template developed with the the latest HTML5 and CSS3 technologies. It can be perfectly fit for any corporate, e-commerce, business, agency or individual website.">




  	<!-- CSS files -->
	<link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600'>
	<link rel="stylesheet" href="inc/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="inc/animate.css">




	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css">




	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">


</head>
<body>





<!-- Global Wrapper -->
<div id="wrapper" class="page9">

	<!-- Header -->
	<?php include("php/header.php"); ?>
	<!-- Page Header -->
<header class="titlebar" style="background-image: url(dist/img/titlebar.jpg); background-size:cover;"></header>
<section class="breadcrumbs breadcrumb_container" style="background:#ea5f5c;">
	<div class="container">
		<div class="row">
			<ol class="breadcrumb by ">
			  <li><a href="#">Home</a></li>
			  <li class="active">#Contatti</li>
			</ol>
		</div>
	</div>
</section>


<section class="contatti-1 large-padding custom-bg parallax">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-9 pad50">
			<h2>ABOUT US</h2>
			Lorem ipsum dolor sit amet,
			consectetur adipiscing elit.
			Cras tempus, orci sed molestie
			hendrerit, justo urna dignissim.
			</div>
			<div class="col-md-8" style="">
				<iframe height=400 src="https://maps.google.com/?ie=UTF8&amp;ll=34.053477,-118.241086&amp;spn=0.031112,0.038581&amp;t=m&amp;z=15&amp;output=embed"></iframe>
			</div>
		</div>
	</div>
</section>

<section class="animare-4 custom-bg parallax">
		<div class="row mr0" style="background:#92ba49;">
			<div class="col-md-6 pad-l-50 pt50">
				<h3>Vuoi maggiori informazioni? Scrivici</h3>
				<p class="cGray lead">
				Lorem ipsum dolor sit amet,
				consectetur adipiscing elit.
				Cras tempus, orci sed molestie
				hendrerit, justo urna dignissim.
				</p>
			</div>
			<div class="col-md-6 pad50">
				<p><input style="background:#92ba49" type="text" value="Nome" /></p>
				<p><input style="background:#92ba49" type="text" value="Cognome" /></p>
				<p><input style="background:#92ba49" type="text" value="Email" /></p>
				<p><input style="background:#92ba49" type="text" value="Telefono" /></p>
				<p><input style="background:#92ba49" type="text" value="Sono" /></p>
				<p><textarea style="background:#92ba49; width:100%" value="Messaggio"> Messaggio</textarea></p>
				<p><input type="checkbox" />Ho preso visione e accetto l'informativa sulla privacy</p>
				<p><a class="btn btn-no-bg pad5 pad-l-20 pad-r-20 pull-right">Invia</a></p>
		</div>
	</div>
</section>

<?php include("php/footer.php"); ?>

</div>





	<!-- Javascript files -->
	<script src="inc/jquery/jquery-2.1.0.min.js"></script>
	<script src="inc/bootstrap/js/bootstrap.min.js"></script>
	<script src="inc/jquery.appear.js"></script>
	<script src="inc/retina.min.js"></script>
	<script src="inc/jflickrfeed.min.js"></script>



	<script src="inc/isotope/isotope.pkgd.min.js"></script>
	<script src="inc/isotope/imagesloaded.pkgd.min.js"></script>


	<!-- Main javascript file -->
	<script src="js/script.js"></script>


</body>
</html>
