<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Voyo | Responsive Multipurpose HTML5 Template</title>

	<!-- Change the author and description -->
	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Voyo One is a multipurpose HTML Template developed with the the latest HTML5 and CSS3 technologies. It can be perfectly fit for any corporate, e-commerce, business, agency or individual website.">




  	<!-- CSS files -->
	<link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600'>
	<link rel="stylesheet" href="inc/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="inc/animate.css">




	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css">




	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">


</head>
<body>





<!-- Global Wrapper -->
<div id="wrapper" class="page6">

	<!-- Header -->
	<?php include("php/header.php"); ?>
	<!-- Page Header -->
<header class="titlebar" style="background-image: url(dist/img/abitare.jpg); background-size:cover;"></header>
<section class="breadcrumbs breadcrumb_container abitarebg">
	<div class="container">
		<div class="row">
			<ol class="breadcrumb by ">
			  <li><a href="#">Home</a></li>
			  <li class="active">#Abitare</li>
			</ol>
		</div>
	</div>
</section>
	<section class="animare-1"> 
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-9 col-md-push-3  space-left pad50">
					<div id="portfolio-iportfoliosotope">
								<div class="" style="text-align:left;">
									<h1 class="main_title abitare-color" >#Abitare</h1>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
									</p>
									<p><i>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.

										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto.

										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto.
									</i></p>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto.
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto.
									</p>
						</div> <!-- END portfolio Container -->
					</div> <!-- END Portfolio Isotope -->

					<!--<ul class="pagination">
						<li class="disabled"><a href="#"><i class="fa fa-chevron-left"></i></a></li>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
					</ul>-->

				</div>
				<div class="col-sm-12 col-md-3 col-md-pull-9 pad50">
					<aside class="sidebar">
						<div class="sidebar-widget">
							<h2 class="sidebar-title mb30 abitare-color">#Abitare</h2>
							<h3 class="sidebar-title mb30 abitare-color">Servizi</h3>
							<ul class="categories">
								<li><a href="#">Animazione di strada</a></li>
								<li><a href="#">Centri giovanili - Casa Novarini</a></li>
								<li><a href="#">Interventi di prevenzione</a></li>
								<li><a href="#">informazione e orientamento</a></li>
								<li><a href="#">supporto scolastico e doposcuola</a></li>
								<li><a href="#">orineta lavoro</a></li>
								<li><a href="#">formazione e laboratori a scuola</a></li>
								<li><a href="#">consulenza sulle politiche giovanili</a></li>
							</ul>
						</div>
						<div class="sidebar-widget">
							<h3 class="sidebar-title abitare-color">I quaderni di Energie Sociali</h3>
							<div class="">
								<div class="card pad20">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, maiores pariatur dolorum tenetur architecto magnam earum debitis ullam accusamus est!
									<br><br>
									<a class="btn btn-no-bg abitare-color">Download</a>
								</div>
							</div>
							<div class="">
								<div class="card pad20">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, maiores pariatur dolorum tenetur architecto magnam earum debitis ullam accusamus est!
									<br><br>
									<a class="btn btn-no-bg abitare-color">Download</a>
								</div>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</section> <!-- END Blog Page-->

	<section class="animare-4 large-padding custom-bg parallax">
			<div class="row mr0" style="background:#ea5f5c;">
				<div class="col-md-6 pad-l-50 pt50">
					<h3>Vuoi maggiori informazioni? Scrivici</h3>
					<p class="cGray lead">
					Lorem ipsum dolor sit amet,
					consectetur adipiscing elit.
					Cras tempus, orci sed molestie
					hendrerit, justo urna dignissim.
					</p>
				</div>
				<div class="col-md-6 pad50">
					<p><input type="text" value="Nome" /></p>
					<p><input type="text" value="Cognome" /></p>
					<p><input type="text" value="Email" /></p>
					<p><input type="text" value="Telefono" /></p>
					<p><input type="text" value="Sono" /></p>
					<p><textarea style="width:100%" value="Messaggio"> Messaggio</textarea></p>
					<p><input type="checkbox" />Ho preso visione e accetto l'informativa sulla privacy</p>
					<p><a class="btn btn-no-bg pad5 pad-l-20 pad-r-20 pull-right">Invia</a></p>
			</div>
		</div>
	</section>

	<section class="blog large-padding mb40 custom-bg parallax">
		<div class="container">
			<h2 class="text-center mb40"><span class="green">Le storie</span><br/><small>#Educare</small></h2>
				<div class="row">
					<div class="col-sm-12 col-md-3 fadeInUp animated">
						<div class="blog-item">
							<div><img src="dist/img/le_storie_1.jpg" alt="service"></div>
								<div class="blog-caption pad0">
									<a href="#" class="btn-block btn_green">Nicola <br> Centro Parsifal</a>
								</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-3 fadeInUp animated">
						<div class="blog-item">
							<div><img src="dist/img/le_storie_2.jpg" alt="service"></div>
								<div class="blog-caption pad0">
									<a href="#" class="btn-block btn_green">Nicola <br> Centro Parsifal</a>
								</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-3 fadeInUp animated">
						<div class="blog-item">
							<div><img src="dist/img/le_storie_3.jpg" alt="service"></div>
								<div class="blog-caption pad0">
									<a href="#" class="btn_green btn-block">Nicola <br> Centro Parsifal</a>
								</div>
						</div>
					</div>
					<div class=" col-sm-12 col-md-3 fadeInUp animated">
						<div class="blog-item">
							<div><img src="dist/img/le_storie_4.jpg" alt="service"></div>
								<div class="blog-caption pad0">
									<a href="#" class="btn_green btn-block">Nicola <br> Centro Parsifal</a>
								</div>
						</div>
					</div>
			</div>
			</div>
	</section>


	<section class="section-5 custom-bg parallax">
			<div class="row mr0" style="background:#92ba49">
				<div class="col-md-6 col-sm-12 pad-l-50 pad-r-50 pt50" style="text-align:right">
					<h2 style="color:white">Sei un Azienda?</h2><br>
					<p class="cGray lead col-md-6 col-sm-12" style="float:right; color:white;">
						Lorem ipsum dolor sit amet,
						consectetur adipiscing elit.
						Cras tempus, orci sed molestie
						hendrerit, justo urna dignissim.<br><br>
						<a href="#" class="btn gc_wb pad-l-20 pad-r-20">SCOPRI COME</a>
					</p>
				</div>
				<div class="col-md-6 pad0">
					<img src="dist/img/educare.jpg" alt="" title="" name="">
			</div>
		</div>
	</section>
	
	<?php include("php/footer.php");?>

</div> <!-- END Global Wrapper -->





	<!-- Javascript files -->
	<script src="inc/jquery/jquery-2.1.0.min.js"></script>
	<script src="inc/bootstrap/js/bootstrap.min.js"></script>
	<script src="inc/jquery.appear.js"></script>
	<script src="inc/retina.min.js"></script>
	<script src="inc/jflickrfeed.min.js"></script>



	<script src="inc/isotope/isotope.pkgd.min.js"></script>
	<script src="inc/isotope/imagesloaded.pkgd.min.js"></script>


	<!-- Main javascript file -->
	<script src="js/script.js"></script>


</body>
</html>
