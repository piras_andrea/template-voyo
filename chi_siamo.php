<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Voyo | Responsive Multipurpose HTML5 Template</title>

	<!-- Change the author and description -->
	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Voyo One is a multipurpose HTML Template developed with the the latest HTML5 and CSS3 technologies. It can be perfectly fit for any corporate, e-commerce, business, agency or individual website.">




  	<!-- CSS files -->
	<link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600'>
	<link rel="stylesheet" href="inc/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="inc/animate.css">




	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css">




	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">


</head>
<body>





<!-- Global Wrapper -->
<div id="wrapper" class="page1">

	<!-- Header -->
	<?php include("php/header.php"); ?>
	<!-- Page Header -->
<header class="titlebar" style="background-image: url(dist/img/titlebar_news.jpg); background-size:cover;"></header>
<section class="breadcrumbs breadcrumb_container" style="background:#ea5f5c;">
	<div class="container">
		<div class="row">
			<ol class="breadcrumb by ">
			  <li><a href="#">Home</a></li>
			  <li class="active">#Chi siamo</li>
			</ol>
		</div>
	</div>
</section>


<section class="contatti-1 large-padding  parallax">
	<div class="container">
		<div class="row">
			<div class="col-md-12 pad20">
				<h1 class="main_title pink" >Chi siamo</h1>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
				</p>
				<p><i>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.

					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto.

					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto.
				</i></p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto.
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto.
				</p>
			</div>
		</div>
	</div>
</section>

<section class="animare-4  parallax">
		<div class="row pad50 mr0" style="background:#92ba49;">
			<div class="col-md-4 col-sm-12 pad-l-50 pt20 xs-box-3">
				<div class="white-box">
					<h2 class="gray" style="font-weight:500">Testimonials</h2><br>
					<p class="pad10 gray">Lorem ipsum dolor sit amet, consectetur as adipisicing elit. Amet, voluptatum porro set reiciendis, magni rerum velit, repudiandae fugit, animi id obcaecati mollitia nemo possimus.</p>
					</div>
				</div>
			<div class="col-md-8 col-sm-12 pad-l-50 pt50">
				<div class="text-center"><i class="fa fa-quote-left fa-2x pink"></i></div>
				<div id="myCarousel" class="carousel slide vertical pad-l-50 pad-r-50">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<div class="active item text-center">
							<p class="white thin">Lorem ipsum dolor sit amet, consectetur as adipisicing elit. Amet, voluptatum porro set reiciendis, magni rerum velit, repudiandae fugit, animi id obcaecati mollitia nemo possimus.</p>
							<h4 class="pink margin0">John Doe</h4>
							<p class="small">VideoHive</p>
						</div>
						<div class="item text-center">
							<p class="white thin">Lorem ipsum dolor sit amet, consectetur as adipisicing elit. Amet, voluptatum porro set reiciendis, magni rerum velit, repudiandae fugit, animi id obcaecati mollitia nemo possimus.</p>
							<h4 class="pink margin0">John Doe</h4>
							<p class="small">VideoHive</p>
						</div>
						<div class="item text-center">
							<p class="white thin">Lorem ipsum dolor sit amet, consectetur as adipisicing elit. Amet, voluptatum porro set reiciendis, magni rerum velit, repudiandae fugit, animi id obcaecati mollitia nemo possimus.</p>
							<h4 class="pink margin0">John Doe</h4>
							<p class="small">VideoHive</p>
						</div>
					</div>
					<!-- Carousel nav -->
					<!--<a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a>
					<a class="carousel-control right" href="#myCarousel" data-slide="next">›</a>-->
				</div>
			</div>
		</div>
	</div>
</section>


<section  class="section4 large-padding custom-bg parallax">
		<div class="pt40 pb40">
<div class="container">
				<h2 class="text-center mb40">I nostri numeri<br/><small>Sottotitolo</small></h2>
				<div class="row">
					<div class="col-md-3 custom-col-20 col-sm-9">
						<!--Shadow circle animated fill-->
						<svg height=200 width=200 id="svg-1">
							 <circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/people.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
					<div class="col-md-3 custom-col-20 col-sm-9">
						<svg height=200 width=200 id="svg-2">
							 <circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/hand.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
					<div class="col-md-3 custom-col-20 col-sm-9">
						<svg height=200 width=200 id="svg-3">
							 <circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/flag.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
					<div class="col-md-3 custom-col-20 col-sm-9">
						<svg height=200 width=200 id="svg-4">
							 <circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/trapano.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
					<div class="col-md-3 custom-col-20 col-sm-9">
						<svg height=200 width=200 id="svg-5">
							<circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/bed.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
				</div>
				<div style="text-align:center">
					<a href="#" class="margin20 ml50 mtr50 btn btn-no-bg pb_pc onhover_wc_pb">APPROFONDISCI</a>
				</div>
			</div>
		</div>
	</section>

<?php include("php/footer.php"); ?>

</div>





	<!-- Javascript files -->
	<script src="inc/jquery/jquery-2.1.0.min.js"></script>
	<script src="inc/bootstrap/js/bootstrap.min.js"></script>
	<script src="inc/jquery.appear.js"></script>
	<script src="inc/retina.min.js"></script>
	<script src="inc/jflickrfeed.min.js"></script>



	<script src="inc/isotope/isotope.pkgd.min.js"></script>
	<script src="inc/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="src/_lib/vivus/dist/vivus.min.js"></script>

	<!-- Main javascript file -->
	<script src="js/script.js"></script>
	<script>
		$('.carousel').carousel({
			interval: 3000
		});

		new Vivus('svg-1', {duration: 200}, myCallback);
		new Vivus('svg-2', {duration: 200}, myCallback);
		new Vivus('svg-3', {duration: 200}, myCallback);
		new Vivus('svg-4', {duration: 200}, myCallback);
		new Vivus('svg-5', {duration: 200}, myCallback);
		function myCallback(){}
	</script>
	

</body>
</html>
