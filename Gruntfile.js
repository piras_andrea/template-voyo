'use strict';

var pkgjson = require('./package.json');
 
var config = {
    pkg: pkgjson,
    app: 'src',
    inc: 'inc',
    dist: 'dist',
    scripts: 'js',
    styles: 'css'
}

module.exports = function(grunt) {
    grunt.initConfig({
        config: config,
        pkg: config.pkg,
        bower: grunt.file.readJSON('./.bowerrc'),

        clean: {
            dist: ['dist']
        },
        less: {
            options: {
                banner: '/*! <%= pkg.name %> lib - v<%= pkg.version %> -' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
            },
            dist: {
                files: [{
                    src: '<%= config.styles %>/less/style-main.less',
                    dest: '<%= config.styles %>/css/style-main.css'
                },
				{
                    src: '<%= config.styles %>/less/animated-circle.less',
                    dest: '<%= config.styles %>/css/animated-circle.css'
                }]
            }
        },
        cssmin: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.styles %>/css',
                    src: ['*.css', '!*.min.css'],
                    dest: '<%= config.dist %>/css',
                    ext: '.min.css'
                }]
            },
            theme: {
                files: [{
                    expand: true,
                    cwd: '<%= config.inc %>/rs-plugin/css',
                    src: ['*.css', '!*.min.css'],
                    dest: '<%= config.dist %>/css/rs-plugin',
                    ext: '.min.css'
                }]
            },
            lib: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/_lib/flexslider',
                    src: 'flexslider.css',
                    dest: '<%= config.dist %>/css',
                    ext: '.min.css'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/magnific-popup/dist',
                    src: 'magnific-popup.css',
                    dest: '<%= config.dist %>/css',
                    ext: '.min.css'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/rangeslider/dist',
                    src: 'rangeslider.css',
                    dest: '<%= config.dist %>/css',
                    ext: '.min.css'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/raty/lib',
                    src: 'jquery.raty.css',
                    dest: '<%= config.dist %>/css',
                    ext: '.min.css'
                }]
            }
        },
        copy: {
            theme: {
                files: [{
                    expand: true,
                    cwd: '<%= config.inc %>',
                    src: 'jquery.nav.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.inc %>/rs-plugin/font',
                    src: '*',
                    dest: '<%= config.dist %>/fonts'
                },
                {
                    expand: true,
                    cwd: '<%= config.inc %>/rs-plugin',
                    src: 'js/jquery.themepunch.tools.min.js',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.inc %>/rs-plugin',
                    src: 'js/jquery.themepunch.revolution.min.js',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.inc %>/sharrre',
                    src: 'jquery.sharrre.min.js',
                    dest: '<%= config.dist %>/js'
                }]
            },
            lib: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/_lib/animate.css',
                    src: 'animate.min.css',
                    dest: '<%= config.dist %>/css'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/bootstrap/dist',
                    src: 'css/bootstrap.min.css',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/bootstrap',
                    src: 'fonts/*',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/bootstrap/dist',
                    src: 'js/bootstrap.min.js',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/Chart.js',
                    src: 'Chart.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/flexslider',
                    src: 'jquery.flexslider-min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/flexslider',
                    src: 'fonts/*',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/fontawesome',
                    src: 'css/font-awesome.min.css',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/fontawesome',
                    src: 'fonts/*',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/isotope/dist',
                    src: 'isotope.pkgd.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/imagesloaded',
                    src: 'imagesloaded.pkgd.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/jquery.countdown/dist',
                    src: 'jquery.countdown.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/jquery-placeholder',
                    src: 'jquery.placeholder.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/jquery-validation/dist',
                    src: 'jquery.validate.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/jquery-zoom',
                    src: 'jquery.zoom.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/magnific-popup/dist',
                    src: 'jquery.magnific-popup.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/owl.carousel/dist',
                    src: 'owl.carousel.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/owl.carousel/dist/assets',
                    src: 'owl.carousel.min.css',
                    dest: '<%= config.dist %>/css'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/rangeslider/dist',
                    src: 'rangeslider.min.js',
                    dest: '<%= config.dist %>/js'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/raty/lib',
                    src: 'fonts/*',
                    dest: '<%= config.dist %>'
                },
                {
                    expand: true,
                    cwd: '<%= config.app %>/_lib/retina.js/dist',
                    src: 'retina.min.js',
                    dest: '<%= config.dist %>/js'
                }]
            }
        },
        imagemin: {
            dist: {
                files: [{
                    expand: true,                   // Enable dynamic expansion
                    cwd: 'images/',                 // Src matches are relative to this path
                    src: ['**/*.{png,jpg,gif,ico}'],    // Actual patterns to match
                    dest: 'dist/img'             // Destination path prefix
                }]
            },
            theme: {
                files: [{
                    expand: true,                   // Enable dynamic expansion
                    cwd: '<%= config.inc %>/rs-plugin/assets',                 // Src matches are relative to this path
                    src: ['*.{png,jpg,gif}'],    // Actual patterns to match
                    dest: 'dist/img/rs-plugin'             // Destination path prefix
                }]
            },
            lib: {
                files: [{
                    expand: true,                   // Enable dynamic expansion
                    cwd: '<%= config.app %>/_lib/owl.carousel/dist/assets',                 // Src matches are relative to this path
                    src: ['*.{png,jpg,gif}'],    // Actual patterns to match
                    dest: 'dist/img/owl.carousel'             // Destination path prefix
                },
                {
                    expand: true,                   // Enable dynamic expansion
                    cwd: '<%= config.app %>/_lib/raty/lib/images',                 // Src matches are relative to this path
                    src: ['*.{png,jpg,gif}'],    // Actual patterns to match
                    dest: 'dist/img/raty'             // Destination path prefix
                }]
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> lib - v<%= pkg.version %> -' + 
                '<%= grunt.template.today("yyyy-mm-dd") %> */'
            },
            dist: {
                files: [{
                    '<%= config.dist %>/js/jquery.appear.min.js': '<%= config.inc %>/jquery.appear.js'
                }]
            },
            theme: {
                files: [{
                    expand: true,
                    cwd: '<%= config.scripts %>',
                    src: ['*.js', '!*.min.js', '!*.lite.js', '!*.minified.js'],
                    dest: '<%= config.dist %>/js',
                    ext: '.min.js'
                }]
            },
            lib: {
                files: [{
                    '<%= config.dist %>/js/jquery.min.js': '<%= bower.directory %>/jquery/dist/jquery.js'
                },
                {
                    '<%= config.dist %>/js/jquery.countTo.min.js': '<%= bower.directory %>/jquery-countTo/jquery.countTo.js'
                },
                {
                    '<%= config.dist %>/js/jquery.vticker.min.js': '<%= bower.directory %>/vticker/jquery.vticker.js'
                },
                {
                    '<%= config.dist %>/js/jquery.raty.min.js': '<%= bower.directory %>/raty/lib/jquery.raty.js'
                }]
            }
        },
        watch: {
            scripts: {
                files: ['Gruntfile.js', '<%= config.app %>/*', '<%= config.scripts %>/*', '<%= config.styles %>/**/*'],
                tasks: ['clean','less','cssmin','copy','imagemin','uglify']
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Qui definirai i task
    grunt.registerTask('default', [
        'clean',
        'copy',
        'cssmin',
        'less',
        'imagemin',
        'uglify',
        'watch'
    ]);
};