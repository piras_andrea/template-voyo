<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Voyo | Responsive Multipurpose HTML5 Template</title>

	<!-- Change the author and description -->
	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Voyo One is a multipurpose HTML Template developed with the the latest HTML5 and CSS3 technologies. It can be perfectly fit for any corporate, e-commerce, business, agency or individual website.">



  	<!-- CSS files -->
	<link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600'>
	<link rel="stylesheet" href="inc/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="inc/animate.css">
	<link rel="stylesheet" href="inc/hover-min.css">

	<link rel="stylesheet" href="inc/rs-plugin/css/settings.css" media="screen">
	<link rel="stylesheet" href="dist/css/flexslider.min.css" media="screen">
	<link rel="stylesheet" href="dist/css/animated-circle.min.css" media="screen">


	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css">





	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">


</head>
<body>





<!-- Global Wrapper -->
<div id="wrapper" class="">


	<!-- Header -->
	<?php include("php/header.php"); ?>

	<div id="main-slider" class="flexslider">
		<ul class="slides">
			<li class="custom-bg" data-bg="dist/img/slideshow/slideshow_1.jpg">
				<div class="bg-layer"></div> <!-- background overlay over the image -->
				<div class="flex-content flex-middle"><!-- content container (top:50%)-->
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-7"><!-- bootstrap column adaption-->
									<h4 class="flex-subtitle fadeInUp animated" data-anim="fadeInUp">Animare</h4>
									<h2 class="flex-title fadeInDown animated">Corsi Low Cost</h2>
									<h3 class="flex-subtitle fadeInUp animated">Lorem ipsum dolor<br/>sit amet, cosectetur</h3>
									<a href="animare.php" class="btn btn-flex">Scopri</a>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li class="custom-bg" data-bg="dist/img/slideshow/slideshow_1.jpg">
				<div class="container">
					<div class="row">
						<div class="bg-layer"></div> <!-- background overlay over the image -->
						<div class="flex-content flex-middle"><!-- content container (top:50%)-->
							<div class="col-sm-12 col-md-7"><!-- bootstrap column adaption-->
								<h4 class="flex-subtitle fadeInUp animated" data-anim="fadeInUp">Educare</h4>
								<h2 class="flex-title fadeInDown animated">Corsi Low Cost</h2>
								<h3 class="flex-subtitle fadeInUp animated">Lorem ipsum dolor<br/>sit amet, cosectetur</h3>
								<a href="educare.php" class="btn btn-flex">Scopri</a>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>

	<section class="blog large-padding mb40 custom-bg parallax">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-3">
					<div class="blog-item">
						<div class="no-overflow"><img class="hvr-grow" src="dist/img/news/news1.jpg" alt="service"></div>
							<div class="blog-caption">
								<span class="date">12 | 06 | 2016</span><br>
								<h3 class="post-title bt3">Cross Browser</h3>
								<p class="sub-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!</p>
								<a href="#" class="btn">Continua</a>
							</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-3">
					<div class="blog-item">
						<div class="no-overflow"><img class="hvr-grow" src="dist/img/news/news2.jpg" alt="service"></div>
						<div>
							<div class="blog-caption">
								<span class="date">12 | 06 | 2016</span><br>
								<h3 class="post-title bt3">Mobile Friendly</h3>
								<p class="sub-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!</p>
								<a href="#" class="btn">Continua</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-3">
					<div class="blog-item">
						<div class="no-overflow"><img class="hvr-grow" src="dist/img/news/news1.jpg" alt="service"></div>
							<div>
								<div class="blog-caption">
									<span class="date">12 | 06 | 2016</span><br>
										<h3 class="post-title bt3">Unlimited colors</h3>
											<p class="sub-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!</p>
											<a href="#" class="btn">Continua</a>
								</div>
						</div>
					</div>
				</div>
				<div class=" col-sm-12 col-md-3">
					<div class="blog-item">
						<div class="no-overflow"><img class="hvr-grow" src="dist/img/news/news2.jpg" alt="service"></div>
							<div>
								<div class="blog-caption">
									<span class="date">12 | 06 | 2016</span><br>
									<h3 class="post-title">Unlimited colors</h3>
									<p class="sub-post-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae!</p>
									<a href="#" class="btn">Continua</a>
								</div>
							</div>
					</div>
				</div>
			</div>
	</section>

<section class="section3 large-padding custom-bg parallax">
		<div class="container">
			<h2 class="text-center mb40">I settori<br/><small>Sottotitolo</small></h2>
			<div class="col-md-3 pad0" style="margin:-1px 0 0 -4px;">
				<div class="row margin0">
					<div class="pad0 text-center settori-image-big settori-image-animare"  style="background-image:url(dist/img/settori/l.jpg);">
						<div class="fillcontainer onhover_overlay_white_dot5 onhover_graytext">
							<a class="btn btn-no-bg pb_pc_wb wb_wc_rb_hover mt50">#ANIMARE</a>	
							<br>
							<div style="width:60%; margin:10px 20%;">
								Lorem ipsum dolor sit amet,
								consectetur adipisicing elit.
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-9 pad0" style="margin:0 0 0 -2px;">
				<div class="row margin0">
					<div class="">
						<div  class="col-md-8 pad0">
							<div class="text-center settori-image settori-image-educare" style="margin:0 0 0 3px; background-image:url(dist/img/settori/tc.jpg);">
								<div class="fillcontainer onhover_overlay_white_dot5 onhover_graytext">
									<a class="btn btn-no-bg pb_pc_wb wb_wc_rb_hover mt50">#ANIMARE</a>	
									<br>
										<div style="width:60%; margin:10px 20%;">
											Lorem ipsum dolor sit amet,
											consectetur adipisicing elit.
										</div>
								
								</div>
							</div>
						</div>
					</div>
					<div class="">
						<div class="col-md-4 pad0">
							<div class="text-center settori-image settori-image-lavorare" style="margin:0px 0 0 3px; background-image:url(dist/img/settori/tr.jpg);">
								<div class="fillcontainer onhover_overlay_white_dot5 onhover_graytext">
									<a class="btn btn-no-bg pb_pc_wb wb_wc_rb_hover mt50">#ANIMARE</a>	
										<div style="width:60%; margin:10px 20%;">
											Lorem ipsum dolor sit amet,
											consectetur adipisicing elit.
										</div>
								
								</div>
							</div>
						</div>
					</div>
					<div class="">
						<div class="col-md-4 pad0">
							<div class="text-center settori-image settori-image-abitare" style="margin:3px 0 0 3px; background-image:url(dist/img/settori/bc.jpg);">
								<div class="fillcontainer onhover_overlay_white_dot5 onhover_graytext">
									<a class="btn btn-no-bg pb_pc_wb wb_wc_rb_hover mt50">#ANIMARE</a>	
										<div style="width:60%; margin:10px 20%;">
											Lorem ipsum dolor sit amet,
											consectetur adipisicing elit.
										</div>
								
								</div>
							</div>
						</div>
					</div>
					<div class="">
						<div class="col-md-8 pad0">
							<div class="text-center settori-image settori-image-formare" style=" margin:3px 0 0 3px; background-image:url(dist/img/settori/br.jpg);">
								<div class="fillcontainer onhover_overlay_white_dot5 onhover_graytext">
									<a class="btn btn-no-bg pb_pc_wb wb_wc_rb_hover mt50">#ANIMARE</a>	
									<br>
										<div style="width:60%; margin:10px 20%;">
											Lorem ipsum dolor sit amet,
											consectetur adipisicing elit.
										</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		<!--<a href="educare.php">
									<div class="btn btn-w-bg">#Educare</div>
									<p>Lorem ipsum dolor<br/>sit amet,<br/>consectetur adipiscing</p>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-9">
					<div class="row">
						<div class="col-md-8 gutter-5-r gutter-5-t gutter-5-b gutter-5-l">
							<div class="text-center sector-bg" style="background-image: url('dist/img/settori/tc.jpg'); height: 180px; background-size: cover; background-position: bottom center;">
								<a href="animare.php">
									<div class="btn btn-w-bg">#Animare</div>
									<p>Lorem ipsum dolor<br/>sit amet,<br/>consectetur adipiscing</p>
								</a>
							</div>
						</div>
						<div class="col-md-4 gutter-5-t gutter-5-b gutter-5-l">
							<div class="text-center pt40 pb40 sector-bg" style="background-image: url('dist/img/settori/tr.jpg'); height: 180px; background-size: cover; background-position: bottom center;">
								<a href="#">
									<div class="btn btn-w-bg">#Lavorare</div>
									<p>Lorem ipsum dolor<br/>sit amet,<br/>consectetur adipiscing</p>
								</a>
							</div>
						</div>
						<div class="col-md-4 gutter-5-r gutter-5-t gutter-5-b gutter-5-l">
							<div class="text-center pt40 pb40 sector-bg" style="background-image: url('dist/img/settori/bc.jpg'); height: 180px; background-size: cover; background-position: top center;">
								<a href="#" class="pt50 pb50">
									<div class="btn btn-w-bg">#Abitare</div>
									<p>Lorem ipsum dolor<br/>sit amet,<br/>consectetur adipiscing</p>
								</a>
							</div>
						</div>
						<div class="col-md-8 gutter-5-t gutter-5-b gutter-5-l">
							<div class="text-center pt40 pb40 sector-bg" style="background-image: url('dist/img/settori/br.jpg'); height: 180px; background-size: cover; background-position: top center;">
								<a href="#" class="pt50 pb50">
									<div class="btn btn-w-bg">#Formare</div>
									<p>Lorem ipsum dolor<br/>sit amet,<br/>consectetur adipiscing</p>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>-->
		</div>
	</section>

	<section  class="section4 large-padding custom-bg parallax">
		<div class="pt40 pb40">
			<div class="container">
				<h2 class="text-center mb40">I nostri numeri<br/><small>Sottotitolo</small></h2>
				<div class="row">
					<div class="col-md-3 custom-col-20 col-sm-9">
						<!--Shadow circle animated fill-->
						<svg height=200 width=200 id="svg-1">
							 <circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/people.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
					<div class="col-md-3 custom-col-20 col-sm-9">
						<svg height=200 width=200 id="svg-2">
							 <circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/hand.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
					<div class="col-md-3 custom-col-20 col-sm-9">
						<svg height=200 width=200 id="svg-3">
							 <circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/flag.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
					<div class="col-md-3 custom-col-20 col-sm-9">
						<svg height=200 width=200 id="svg-4">
							 <circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/trapano.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
					<div class="col-md-3 custom-col-20 col-sm-9">
						<svg height=200 width=200 id="svg-5">
							<circle cx="100" cy="100" r="90" stroke="#ddd" stroke-width="12" fill="#fff" />
						</svg>
						<img class="svg_contained img150 rounded animated" data-anim="fadeIn" src="dist/img/icons/es/bed.jpg" alt="">
						<h2 class="mt10 mb20">46.193</h2>
						<p class="wu">Presenze annuali<br/>nelle attività gestite</p>
					</div>
				</div>
				<div style="text-align:center">
					<a href="#" class="margin20 ml50 mtr50 btn btn-no-bg pb_pc onhover_wc_pb">APPROFONDISCI</a>
				</div>
			</div>
		</div>
	</section>

	<section class="section5 large-padding custom-bg parallax">
		<div class="row animated mr0" style="background:#ea5f5c" style="margin:0" data-anim="fadeInRight">
			<div class="col-md-6 col-sm-12 pad-l-50 pad-r-50 pt50" style="text-align:right">
				<h2>Le storie</h2><br>
				<p class="cGray lead col-md-6 col-sm-12 ">
				Lorem ipsum dolor sit amet,
				consectetur adipiscing elit.
				Cras tempus, orci sed molestie
				hendrerit, justo urna dignissim.<br><br>
				<a href="#" class="btn pc_wb pad-l-20 pad-r-20">VEDI TUTTE</a>
				</p>
			</div>
			<div class="col-md-6 pad0">
				<img  src="dist/img/le_storie.jpg" alt="" title="" name="">
			</div>
		</div>
		<div class="row animated mr0" style="background:#ebc676" data-anim="fadeInLeft">
			<div class="col-md-6 pad0">
				<img src="dist/img/volontario.jpg" alt="" title="" name="">
			</div>
			<div class="col-md-6 col-sm-12 pad-l-50 pt50">
				<h2>Le storie</h2>
				<p class="cGray lead col-md-6 col-sm-12">
				Lorem ipsum dolor sit amet,
				consectetur adipiscing elit.
				Cras tempus, orci sed molestie
				hendrerit, justo urna dignissim.<br><br>
				<a href="#" class="btn yc_wb pad-l-20 pad-r-20">SOSTIENICI</a>
				</p>
			</div>
	</div>
	</section>

	<section class="section6 pt20 pb50">
		<div class="container">
			<div >
				<h4>Energie Sociali</h4>
				<p>
		Ut nec turpis malesuada, porta augue nec, venenatis dolor. Aliquam ut vehicula eros. Nunc dapibus ante vel sapien commodo, ac pulvinar ex convallis. Aliquam pharetra hendrerit turpis sed placerat. Integer eu nunc ac tellus condimentum pulvinar. Ut sed diam id purus laoreet condimentum. Aliquam leo nibh, fermentum non iaculis quis, aliquam eu elit. Quisque tristique lectus at aliquam pretium. Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse odio dolor, consequat ut aliquet at, suscipit eu sem. Aliquam tempus tristique mauris, sit amet eleifend ante iaculis et. Nullam in sagittis ipsum. Cras volutpat pellentesque egestas. Etiam sit amet leo mattis, consequat leo ac, laoreet augue. Nam volutpat magna et posuere faucibus.
				</p>
			</div>
		</div>
	</section>
	
	<?php include_once('php/footer.php'); ?>
	
			</div> <!-- END Global Wrapper -->
			<!-- Javascript files -->
			<script src="dist/js/jquery.min.js"></script>
			<script src="dist/js/bootstrap.min.js"></script>
			<script src="dist/js/jquery.flexslider-min.js"></script>
			<script src="dist/js/jquery.appear.min.js"></script>
			<script src="dist/js/retina.min.js"></script>
			<script src="dist/js/jquery.countTo.min.js"></script>
			<script src="src/_lib/vivus/dist/vivus.min.js"></script>
			<!-- Main javascript file -->
			<script src="dist/js/script.min.js"></script>
			<script>
				new Vivus('svg-1', {duration: 200}, myCallback);
				new Vivus('svg-2', {duration: 200}, myCallback);
				new Vivus('svg-3', {duration: 200}, myCallback);
				new Vivus('svg-4', {duration: 200}, myCallback);
				new Vivus('svg-5', {duration: 200}, myCallback);
				function myCallback(){}
			</script>
			
			
		</body>
	</html>
