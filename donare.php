<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Voyo | Responsive Multipurpose HTML5 Template</title>

	<!-- Change the author and description -->
	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Voyo One is a multipurpose HTML Template developed with the the latest HTML5 and CSS3 technologies. It can be perfectly fit for any corporate, e-commerce, business, agency or individual website.">



  	<!-- CSS files -->
	<link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600'>
	<link rel="stylesheet" href="inc/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="inc/animate.css">


	<link rel="stylesheet" href="inc/rs-plugin/css/settings.css" media="screen">


	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="css/style.css">





	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">


</head>
<body>





<!-- Global Wrapper -->
<div id="wrapper">


	<!-- Header -->
	<?php include("php/header.php"); ?>

	<section class="donare-main-image custom-bg parallax rs_fullwidth" style="margin-top: -64.2px">
		<div class="custom-bg donare-main-image-container pt20" data-bg="dist/img/abitare.jpg">
			<div class="row mr0">
				<div class="col-sm-12 col-md-12 pad50" style="height:40%">
					<div class="pad50">
					<h4 class="white" >DONA ORA CON PAYPAL</h4>
					<h2><strong class="white">Insieme possiamo <br> aiutare molte persone</strong></h2>
					</div>
				</div>
				<div class="col-sm-12 col-md-12 pad50 darkgray text-center" style="margin-top:0%; background:rgba(255,255,255,0.5);"><!-- bootstrap column adaption-->
					<div class="pad50">
						<strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</strong> Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
						</br>
						</br>
					
						<span class="pad20 pink">
							<strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</strong>
						</span>
						<br>
						<br>
						<span class="price-list">
							<ul style="">
								<li><h2>5&euro;</h2></li>
								<li><h2>| 10&euro;</h2></li>
								<li><h2>| 20&euro;</h2></li>
								<li><h2>| 50&euro;</h2></li>
								<li><h2>| 100&euro;</h2></li>
							</ul>
						</span>
						<p class="pt20">
							<button class="btn btn-lg pb_pc btn-no-bg pc_wb_hover">DONA ORA CON PAYPAL</button>
						</p>
						
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="blog large-padding mb40 custom-bg parallax pinkbg">
		<div class="container">
			<div class="row mr0">
				<div class="col-sm-12 col-md-12 text-center">
					<div class="pad50">
						<h2><strong class="white">Il tuo 5x1000 a Energie Sociali</strong></h2>
					</div>
				</div>
			</div>
			<div>
				<div class="col-sm-12 col-md-6">
					<div class="pad50 white">
						<strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</strong> Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga, similique, facilis dolorum aliquid amet quasi eveniet repellendus incidunt iste placeat optio cum blanditiis ipsum quia eligendi dolorem quo commodi velit architecto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, labore ipsum eos aspernatur vero quas nostrum temporibus laborum sit numquam. Deleniti, doloribus, velit, eaque a libero temporibus facilis ea quis eum totam assumenda facere voluptas molestias impedit fugiat nemo vel voluptate consequuntur est sapiente porro itaque suscipit placeat vitae nisi.
						<br><br>
						<a class="btn wb_wc_pb">SCOPRI I BENEFICI FISCALI</a>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="pad50">
						<div><img src="dist/img/news/news1.jpg" alt="service"></div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="section6 pt50 pb50">
		<div class="container">
			<div >
				<h4>Energie Sociali</h4>
				<p>
		Ut nec turpis malesuada, porta augue nec, venenatis dolor. Aliquam ut vehicula eros. Nunc dapibus ante vel sapien commodo, ac pulvinar ex convallis. Aliquam pharetra hendrerit turpis sed placerat. Integer eu nunc ac tellus condimentum pulvinar. Ut sed diam id purus laoreet condimentum. Aliquam leo nibh, fermentum non iaculis quis, aliquam eu elit. Quisque tristique lectus at aliquam pretium. Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse odio dolor, consequat ut aliquet at, suscipit eu sem. Aliquam tempus tristique mauris, sit amet eleifend ante iaculis et. Nullam in sagittis ipsum. Cras volutpat pellentesque egestas. Etiam sit amet leo mattis, consequat leo ac, laoreet augue. Nam volutpat magna et posuere faucibus.
				</p>
			</div>
		</div>
	</section>
	
	
	<?php include_once('php/footer.php'); ?>
	
			</div> <!-- END Global Wrapper -->
			<!-- Javascript files -->
			<script src="dist/js/jquery.min.js"></script>
			<script src="dist/js/bootstrap.min.js"></script>
			<script src="dist/js/jquery.flexslider-min.js"></script>
			<script src="dist/js/jquery.appear.min.js"></script>
			<script src="dist/js/retina.min.js"></script>
			<script src="dist/js/jquery.countTo.min.js"></script>
			<!-- Main javascript file -->
			<script src="dist/js/script.min.js"></script>
		</body>
	</html>
