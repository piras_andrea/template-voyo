<div class="footer-wrapper">
	<div class="main-footer">
		<div class="container text-center mt50">
			<div class="row">
				<div class="col-md-12">
					<p><h2 class="footer-title">Sostieni energie sociali</h2></p>
					<p><button class="btn btn-lg wb_wc_pb pc_wb_hover">DONA ORA CON PAYPAL</button></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="footer-menu list-inline text-center">
						<li><a href="chi_siamo.php">Chi Siamo</a></li>
						<li><a href="le_storie.php">Le storie</a></li>
						<li><a href="educare.php">#Educare</a></li>
						<li><a href="animare.php">#Animare</a></li>
						<li><a href="lavorare.php">#Lavorare</a></li>
						<li><a href="abitare.php">#Abitare</a></li>
						<li><a href="formare.php">#Formare</a></li>
						<li><a href="news.php">News</a></li>
						<li><a href="contatti.php">Contatti</a></li>
					</ul>
				</div>
			</div>
			<div class="row mb50">
				<div class="col-md-12">
					<ul class="social-icon dark-2 circle">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
					</ul> <!-- END Social Media -->
				</div>
			</div>
			
		</div> <!-- END Container -->
	</div> <!-- END Main-Header -->
	<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="pull-left">
						<p>
							<small>
							<strong>ENERGIE SOCIALI - Via XX Settembre, 21 - 37129 Verona - P.IVA 03784010237</strong><br>
							tel +39 045 8013824 - fax +39 045 8356017 segreteria@energiesociali.it - energiesociali@postecert.it - credit - WEBMAI
							</small>
						</p>
					</div>
					<div class="pull-right">
						<a href="donare.php" class="btn btn-no-bg">
							Inscriviti alla Newsletter
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>