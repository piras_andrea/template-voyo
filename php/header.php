
<div class="top-bar">
	<div class="container">
		<div class="row">
			<div class="col-sm-5">
				<!-- Social Media Top Bar -->
				<ul class="social-icon white circle">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
				</ul> <!-- END Social Media -->
				<!--<img src="images/logo.png" alt="Logo Voyo One"> -->
			</div>
			<div class="col-md-2 text-center">
				<a href="index.php" title="">
					<img src="dist/img/logo.jpg" alt="" title="" name="" />
				</a>
			</div>
			<div class="col-sm-5">
				<div class="col-md-12">
					<ul class="pull-right top-menu">
						<li><a href="privati.php">Cittadini</a></li>
						<li><a href="#">Aziende</a></li>
						<li><a href="#">Istruzioni</a></li>
					</ul>
				</div>
				<div class="col-md-12 text-right">
					<a href="donare.php" class="btn btn-no-bg">SOSTIENICI</a>
				</div>
			</div>
		</div>
	</div> <!-- END Container -->
</div> <!-- END Main-Header -->
<header class="header-wrapper">
	<div class="main-header">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 menu-bar">
					<ul class="list-inline menu text-center">
						
						<!-- Toggle Menu - For Mobile Devices -->
						<li class="toggle-menu">
							<span class="title">Menu</span>
							<span class="icon"><i></i><i></i><i></i></span>
						</li> <!-- END Toggle Menu -->
						
						<li class="darkgray"><a href="chi_siamo.php">Chi Siamo</a></li>
						<li class="darkgray"><a href="le_storie.php">Le storie</a></li>
						<li class="pink green-hover green-border-hover"><a class="" href="educare.php">#Educare</a></li>
						<li class="pink yellow-hover yellow-border-hover"><a class="" href="animare.php">#Animare</a></li>
						<li class="pink lavorare-color-hover lavorare-color-border-hover"><a class="" href="lavorare.php">#Lavorare</a></li>
						<li class="pink abitare-color-hover abitare-color-border-hover"><a class="" href="abitare.php">#Abitare</a></li>
						<li class="pink formare-color-hover formare-color-border-hover"><a class="" href="formare.php">#Formare</a></li>
						<li class="darkgray"><a href="news.php">News</a></li>
						<li class="darkgray"><a href="contatti.php">Contatti</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>